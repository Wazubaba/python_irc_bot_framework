from . import base_commands, networking, settings
import importlib
import signal


class Controller (object):
	"""
	Controller is responsible for managing all of the networks the bot is connected to, as well as initialization and final
	shutdown.
	"""
	def __init__(self):
		self.networks = []
		self.configuration = settings.Config()

		signal.signal(signal.SIGINT, self._sig_int_handler)

	def add_connection(self, network: networking.Network):
		"""
		Add a new network connection

		:param network: the network you want to connect to.
		:type network: networking.Network
		"""
		self.networks.append(network)

	def shutdown(self):
		"""
		Disconnect all networks and terminate their threads.
		"""
		for network in self.networks:
			network.disconnect()
			network.join()

	def start(self):
		"""
		Start all the networks registered under this core.
		"""
		for network in self.networks:
			network.start()

	def load_configuration(self, path: str, debug=False):
		"""
		Load all internal data from a json-style configuration file.

		:param path: Path to the config file
		:type path: str
		:param debug: Whether to load debug commands as well
		:type debug: bool
		"""

		self.configuration.load_file(path)

		for network_info in self.configuration.networks:
			new_network = networking.Network(network_info)
			if network_info.use_base_functions:
				base_commands.register_base_commands(new_network, network_info.base_command_marker)
				if debug:
					base_commands.register_debug_commands(new_network, network_info.base_command_marker)

			if network_info.script:
				self._resolve_functions(new_network, network_info)

			self.add_connection(new_network)

	def _sig_int_handler(self, *_):
		self.shutdown()

	@staticmethod
	def _resolve_functions(network, network_info):
		script = importlib.import_module(network_info.script)
		for command in network_info.on_connection_callbacks:
			func = getattr(script, command)
			network.register_connection_function(func)

		for command in network_info.on_message_callbacks:
			func = getattr(script, command)
			network.register_message_function(func)

		for command in network_info.on_command_callbacks:
			func = getattr(script, command["function"])
			network.register_command_function(func, command["marker"])

		for command in network_info.on_disconnect_callbacks:
			func = getattr(script, command)
			network.register_disconnect_function(func)
