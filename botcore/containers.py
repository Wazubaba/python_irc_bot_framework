"""
	This module contains numerous containers for grouped data.

	It doesn't really have any functionality beyond that and maybe a handful of helpers
"""


class Message:
	"""
	A container for all of the various elements of an IRC message
	"""
	def __init__(self):
		self.user = ""
		self.host = ""
		self.channel = ""
		self.contents = ""

	def reset(self):
		"""
		Reset a :class:`Message` to empty values.
		"""
		self.user = ""
		self.host = ""
		self.channel = ""
		self.contents = ""


class NetInfo:
	"""
	Contains all the information needed for a single IRC server connection.
	"""
	def __init__(self):
		# Base info
		self.name = ""
		self.host_name = ""
		self.real_name = ""
		self.server_name = ""

		# Host info
		self.host = ""
		self.port = 0
		self.channels = []

		# Scripting settings
		self.script = ""
		self.use_base_functions = False,
		self.base_command_marker = ""

		# Misc stuff
		self.permissions_file = ""

		# Callbacks
		self.on_connection_callbacks = []
		self.on_message_callbacks = []
		self.on_command_callbacks = []
		self.on_disconnect_callbacks = []

	def default(self):
		"""
		Default all data to whatever self.name is. Used for when the command USER is issued.
		"""
		self.host_name = self.name
		self.real_name = self.name
		self.server_name = self.name
