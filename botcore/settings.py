import json
from codecs import open
from . import containers


class ConfigParseError(Exception):
	"""
	Exception thrown when Config fails to parse a file.
	"""


class Config:
	def __init__(self):
		self.networks = []

	def write_file(self, path: str):
		"""
		Output the current config to file `path`.

		:param path: Path to where to write to.
		:type path: str
		"""
		database = []

		for network in self.networks:
			new_network = {
				"name": network.name,
				"real name": network.real_name if network.real_name != network.name else "",
				"host name": network.host_name if network.host_name != network.name else "",
				"host": network.host,
				"port": network.port,
				"channels": network.channels,
				"permissions file": network.permissions_file,
				"script": network.script,
				"subscribe to base functions": network.use_base_functions,
				"base command marker": network.base_command_marker,
				"command functions": network.on_command_callbacks,
				"message functions": network.on_message_callbacks,
				"connection functions": network.on_connection_callbacks,
				"disconnect functions": network.on_disconnect_callbacks
			}

			database.append(new_network)

		try:
			with open(path, "w", "utf-8") as fp:
				json.dump(database, fp)

		except OSError as e:
			print("Error writing '%s': %s" % (e.filename, e.strerror))
			raise e

	def load_file(self, path: str):
		"""
		Load definitions from the json file at `path`

		:param path: Path to the json file to load.
		:type path: str
		"""
		try:
			with open(path, "r", "utf-8") as fp:
				database = json.load(fp)
		except OSError as e:
			print("Error reading '%s': %s" % (e.filename, e.strerror))
			raise e

		# Sanity check: is this even a valid file?
		if not type(database) is list:
			raise ConfigParseError("Error parsing config '%s': Malformed structure" % path)

		for network in database:
			new_network = containers.NetInfo()

			# Grab absolutely required values
			try:
				self._type_test(network, "name", str)
				new_network.name = network["name"]

				self._type_test(network, "host", str)
				new_network.host = network["host"]

				self._type_test(network, "port", int)
				new_network.port = network["port"]

			except TypeError:
				raise ConfigParseError(
					"Error parsing config '%s': Error processing network %s" % (path, database.index(network)))

			except KeyError:
				raise ConfigParseError(
					"Error parsing config '%s': Missing required key(s) for network %s" % (path, database.index(network)))

			# Now default basic info, in case it is not defined later
			new_network.default()

			for entry in network:
				if entry == "real name":
					self._type_test(network, "real name", str)
					new_network.real_name = network["real name"]

				elif entry == "host name":
					self._type_test(network, "host name", str)
					new_network.host_name = network["host name"]

				elif entry == "channels":
					self._type_test(network, "channels", list)
					new_network.channels = network["channels"]

				elif entry == "permissions file":
					self._type_test(network, "permissions file", str)
					new_network.permissions_file = network["permissions file"]

				elif entry == "script":
					self._type_test(network, "script", str)
					new_network.script = network["script"]

				elif entry == "subscribe to base functions":
					self._type_test(network, "subscribe to base functions", bool)
					new_network.use_base_functions = network["subscribe to base functions"]

				elif entry == "base command marker":
					self._type_test(network, "base command marker", str)
					new_network.base_command_marker = network["base command marker"]

				elif entry == "command functions":
					self._type_test(network, "command functions", list)
					new_network.on_command_callbacks = network["command functions"]

				elif entry == "message functions":
					self._type_test(network, "message functions", list)
					new_network.on_message_callbacks = network["message functions"]

				elif entry == "connection functions":
					self._type_test(network, "connection functions", list)
					new_network.on_connection_callbacks = network["connection functions"]

				elif entry == "disconnect functions":
					self._type_test(network, "disconnect functions", list)
					new_network.on_disconnect_callbacks = network["disconnect functions"]

			self.networks.append(new_network)

	@staticmethod
	def _type_test(database, name: str, required_type):
		if not type(database[name]) is required_type:
			raise TypeError("Invalid type for '%s' specified" % name)
