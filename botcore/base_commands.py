"""
Provides a collection of basic commands that would tend to be used by almost any irc bot.
"""


def shutdown(network):
	if network.authorized_users.authorized(network.last_message.host, 4):
		network.disconnect()


def ping(network):
	network.message(network.last_message.channel, "%s: pong!" % network.last_message.user)


# Debug commands
def debug_auth(network):
	print("Current: %s" % network.last_message.host)
	if network.authorized_users.authorized(network.last_message.host):
		print("Is authorized!")
	else:
		print("Is not authorized!")

	for item in network.authorized_users.database:
		print(item)


def register_base_commands(network, marker="!"):
	network.register_command_function(shutdown, "%sshutdown" % marker)
	network.register_command_function(ping, "%sping" % marker)


def register_debug_commands(network, marker="!"):
	network.register_command_function(debug_auth, "%sdebug-auth" % marker)