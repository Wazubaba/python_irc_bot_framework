import socket
import threading
import time

from types import FunctionType

from . import permissions
from .containers import Message, NetInfo


class Network (threading.Thread):
	"""
	Provides an interface to a single network.
	"""

	def __init__(self, network_info: NetInfo):
		super(Network, self).__init__()

		self.info = network_info

		self.socket = socket.socket()
		self.buffer = ""

		self.connection_state = 0

		self.last_message = Message()

		self.authorized_users = permissions.AuthDatabase()

		if self.info.permissions_file:
			self.authorized_users.load_file(self.info.permissions_file)
		else:
			self.authorized_users.append("*", 9999, "Generic Permission User")

		self.command_functions = {}
		self.on_connection_functions = []
		self.on_disconnect_functions = []
		self.on_message_functions = []

	def register_connection_function(self, func: FunctionType):
		"""
		Register a connection to run after initial connection.

		:param func: Function to call
		:type func: function
		"""
		self.on_connection_functions.append(func)

	def register_disconnect_function(self, func):
		"""
		Register a connection to run before disconnecting.

		:param func: Function to call
		:type func: function
		"""
		self.on_disconnect_functions.append(func)

	def register_command_function(self, func, command: str):
		"""
		Register a function called whenever a specific command is found.
		The function should take a single argument - a reference to the network itself.

		:param func: Function to call
		:type func: function
		:param command: Command used to invoke the function
		:type command: str
		"""
		if not command:
			raise AttributeError("Need an actual command here")
		self.command_functions[command] = func

	def register_message_function(self, func):
		"""
		Register a function called whenever a message occurs where the bot can see it.

		:param func: Function to call.
		:type func: function
		"""
		self.on_message_functions.append(func)

	def read(self):
		"""
		Read from the active socket.
		"""
		self.last_message.reset()
		self.buffer = self.socket.recv(512).decode("utf-8")

		if self.buffer == "":
			self.connection_state = 0

	def send(self, message):
		"""
		Send a raw message with proper line-endings to the server.

		:param message: Message to send.
		:type message: str
		"""
		data = "%s\r\n" % message
		self.socket.send(data.encode())

	def run(self):
		"""
		Run is called internally by the threading implementation. Starts processing and managing the internal socket.
		"""
		retries = 0

		while self.connection_state == 0:
			retries += 1

			self.socket.connect((self.info.host, self.info.port))
			self.connection_state = 1

		# Handle login to the server
		while self.connection_state == 1:
			self.read()
			if self._keep_alive(): continue

			if not self.buffer:
				self.connection_state = 0

			self.send("NICK %s" % self.info.name)
			self.send("USER %s %s %s :%s" % (self.info.name, self.info.host_name, self.info.host, self.info.real_name))

			self.connection_state = 2

		# Handle joining the channels once login is complete
		while self.connection_state == 2:
			self.read()
			if self._keep_alive(): continue

			if self.buffer.find("376") != -1:
				self._handle_simple_callback_list(self.on_connection_functions, delay=0.25)
				for channel in self.info.channels:
					self.send("JOIN %s" % channel)

				self.connection_state = 3

		# Handle normal message parsing
		while self.connection_state == 3:
			self.read()
			if self._keep_alive(): continue

			if self.buffer.find("PRIVMSG") != -1:
				try:
					self.last_message.contents = self.buffer.split("PRIVMSG", 1)[1].split(':', 1)[1].strip("\r\n")
				except IndexError:
					self.last_message.contents = "nullmsg"

				try:
					self.last_message.user = self.buffer.split('!')[0][1:]
				except IndexError:
					self.last_message.user = "nullusr"

				try:
					if self.buffer.split()[2].find('#') != -1:
						self.last_message.channel = self.buffer.split()[2]

					if self.last_message.channel[0] == ':': self.last_message.channel = self.last_message.channel[1:]
				except IndexError:
					self.last_message.channel = "nullchn"

				try:
					self.last_message.host = self.buffer.split()[0].split('!')[1].split('@')[1]
				except IndexError:
					self.last_message.host = "nullhst"

			if not self._handle_command_callback_list():
				self._handle_simple_callback_list(self.on_message_functions)

	def disconnect(self):
		"""
		Disconnect the socket after calling the disconnect handlers.
		"""
		self._handle_simple_callback_list(self.on_disconnect_functions, delay=0.25)
		self.connection_state = -1
		self.socket.shutdown(socket.SHUT_RDWR)
		self.socket.close()

	def join_channel(self, channel=""):
		"""
		Join a target channel.

		:param channel: Name of the channel to connect to (ex: "#channel").
		:type channel: str
		"""
		self.send("JOIN %s" % channel)
		self.info.channels.append(channel)

	def part_channel(self, channel=""):
		"""
		Leave a target channel.

		:param channel: Name of the channel to leave (ex: "#channel").
		:type channel: str
		"""
		if channel in self.info.channels:
			self.send("PART %s" % channel)
			self.info.channels.remove(channel)
		else:
			raise IndexError("Invalid channel specified")

	def broadcast_message(self, message):
		"""
		Send a chat message to all channels the bot is in.

		:param message: Message to send.
		:type message: str
		"""
		for channel in self.info.channels:
			self.message(channel, message)

	def broadcast_notice(self, message):
		"""
		Send a notice message to all channels the bot is in.

		:param message: Message to send
		:type message: str
		"""
		for channel in self.info.channels:
			self.notice(channel, message)

	def message(self, target, message):
		"""
		Send a message to a target user or channel.

		:param target: Target user or channel.
		:type target: str
		:param message: Message to send.
		:type message:str
		"""
		self.send("PRIVMSG %s :%s" % (target, message))

	def notice(self, target, message):
		"""
		Send a notice to a target user or channel.

		:param target: Target user or channel.
		:type target: str
		:param message: Message to send.
		:type message: str
		"""
		self.send("NOTICE %s :%s" % (target, message))

	def _keep_alive(self):
		if self.buffer.find("PING") != -1:
			self.send("PONG %s" % (self.buffer.split(" ", 1)[1]))
			return True
		return False

	def _handle_simple_callback_list(self, callbacks, delay=0.0):
		for callback in callbacks:
			try:
				callback(self)
				if delay:
					time.sleep(delay)

			except Exception as e:
				print("[!!]Error executing callback function '%s':\n%s" % (self.command_functions[callback].__name__, e.args))

	def _handle_command_callback_list(self):
		for callback in self.command_functions:
			command_len = len(callback)
			if len(self.last_message.contents) >= command_len:
				if self.last_message.contents[0:command_len] == callback:
					try:
						self.command_functions[callback](self)
						return True

					except Exception as e:
						print("[!!]Error executing callback function '%s':\n%s" % (self.command_functions[callback].__name__, e.args))

		return False
