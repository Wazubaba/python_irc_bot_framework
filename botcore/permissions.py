import fnmatch
import json

# A permissions file consists of a whitelist of host names supporting wildcards, with one host per a line.


class AuthDatabase(object):
	def __init__(self):
		self.database = []

	def get_host_info(self, host):
		for authorized_host in self.database:
			if fnmatch.fnmatch(host, authorized_host["host"]):
				return authorized_host

	def authorized(self, host, level=0):
		# If the host matches something, just go with it
		for authorized_host in self.database:
			if fnmatch.fnmatch(host, authorized_host["host"]):
				if authorized_host["level"] >= level:
					return True

		return False

	def append(self, host, level=0, comment=""):
		self.database.append({"host": host, "level": level, "comment": comment})

	def remove(self, host):
		for authorized_host in self.database:
			if authorized_host == host:
				self.database.remove(authorized_host)
				return

	def modify(self, host, new_host, new_level=-1, new_comment=""):
		for authorized_host in self.database:
			if authorized_host == host:
				authorized_host["host"] = new_host
				if new_level > -1:
					authorized_host["level"] = new_level
				if new_comment != "":
					authorized_host["comment"] = new_comment

	def load_file(self, path):
		with open(path, "r") as fp:
			self.database = json.load(fp)

	def save_file(self, path):
		with open(path, "w") as fp:
			json.dump(self.database, fp)
