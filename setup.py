from setuptools import setup, find_packages

from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README"), encoding="utf-8") as fp:
	long_description = fp.read()

setup(
	name='wazu-bot_core',
	version='1.1.0',
	description='Simple package for writing bots, currently only for IRC however',
	long_description=long_description,
	license='LICENSE.txt',
	author='Wazubaba',
	url='wazu.info.tm',
	author_email='N/A',
	classifiers=[
		"Development Status :: 4 - Beta",
		"Intended Audience :: Developers",
		"License :: OSI Approved :: GNU Library or Lesser General Public License (LGPL)",
		"Operating System :: OS Independent",
		"Programming Language :: Python 3.6",
		"Topic :: Software Development :: Libraries",
		"Topic :: Software Development :: Networking",
		"Topic :: Software Development :: AI"
	],
	packages=find_packages(exclude=["examples"]),
	install_requires=[],
	python_requires=">=3.6",

)
