"""
Demonstrate using the library where everything is defined from the configuration.
The chat commands are located in command_definitions.py.
"""

# settings are to be inherited in order of : commandline<-config<-defaults
from botcore import botcore

if __name__ == "__main__":
	bot = botcore.Controller()
	bot.load_configuration("etc/config.json")
	bot.start()
