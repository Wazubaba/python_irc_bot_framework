def test_func(network):
	network.message(network.last_message.channel, "test_func_1: success! :D")


def example_throwing_function(*_):
	raise Exception("Successfully failed!")


def chat_show(network):
	print("%s | <%s> %s" % (network.last_message.channel, network.last_message.user, network.last_message.contents))


def part_message(network):
	network.broadcast_message("Bye \o")


def connect_message(network):
	print("Connected to %s :D" % network.info.host)
