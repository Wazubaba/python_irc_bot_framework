= Python IRC Bot Framework =

Every so often I sit down and rewrite my older libraries.

It's been a while since I wrote python, having been dabbling in D and C
whilst learning more about programming, so the time has finally come for
my botcore to get a very, very much needed upgrade.


== Dependencies ==
No external libraries.
Built and tested on python 3.6.4.


== Usage ==
After editing `etc/config.json`, you should be able to simply execute
`python main.py`.


== New Features ==
*   Register callbacks to make it so all you have to do is write
some basic logic and some json configs, letting the base do virtually
all of the heavy lifting, or you can just hack it into something else
at will.

*   Includes optional base commands to remove the need to re-code the same
basic functions over and over.

*   Run multiple bots from one instance on separate threads,
or the same bot on several different networks. You can even have different
commands available depending on what network someone is interacting with the
bot from.

*   Improved permissions system to have a concept of power levels, rather
than simply having a flat "is user authorized" one. This is useful to
restrict low-level control functions (like !shutdown) to only the bot's
owner.

*   Improved configuration system to allow config-based editing of what
commands should resolve to what functions.


== TODO ==
*   Torture test the core.

*   Add more built-in functions, or even generic things (think quote
databases or some kind of services-style stuff)

*   Implement some method of command-line arguments to override things,
such as taking a page from `iptables` and sort of having a meta programming
language to take the place of the configuration file, as well as to simply
just specify the usual stuff, like only connecting to a specific net and
channel, or altering where to load the config file from.

